package a02a.e1;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class SequenceAcceptorImpl implements SequenceAcceptor {

    private Sequence myKind;
    private static Map<Sequence, Function<Integer, Integer>> myFunc;
    private int next;
    private int iteration;
    
    static {
        myFunc = new HashMap<>();
        myFunc.put(Sequence.FIBONACCI, i -> fibonacci(i + 1));
        myFunc.put(Sequence.FLIP, i -> i % 2 == 0 ? 1 : 0);
        myFunc.put(Sequence.POWER2, i -> (int) Math.pow(2, i));
        myFunc.put(Sequence.RAMBLE, i -> i % 2 == 0 ? 0 : (i + 1) / 2);
    }

    public SequenceAcceptorImpl() {
        this.myKind = null;
        this.next = -1;
        this.iteration = 1;
    }

    @Override
    public void reset(Sequence sequence) {
        this.myKind = sequence;
        this.next = sequence == Sequence.RAMBLE ? 0 : 1;
        this.iteration = 1;
    }

    @Override
    public void reset() {
        if (this.myKind == null) {
            throw new IllegalStateException();
        }
        this.reset(this.myKind);
    }

    @Override
    public void acceptElement(int i) {
        if (this.next != i || this.myKind == null) {
            throw new IllegalStateException();
        }
        this.next = myFunc.get(this.myKind).apply(iteration);
        this.iteration++;
    }
    
    private static int fibonacci(int iteration) {
        return iteration <= 2 ? 1 : fibonacci(iteration - 1) + fibonacci(iteration - 2);
    }
}
