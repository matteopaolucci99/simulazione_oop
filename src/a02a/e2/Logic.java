package a02a.e2;

public interface Logic {
    boolean hit(int x, int y);
    
    boolean isFinished();
}
