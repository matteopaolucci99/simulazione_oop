package a02a.e2;

import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 70923817406855375L;
    private final Map<JButton, Pair<Integer, Integer>> buttons;
    private final Logic logic;

    public GUI(int size) {
        final JPanel pan = new JPanel(new GridLayout(size, size));
        this.buttons = new HashMap<>();
        this.logic = new LogicImpl(size);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.getContentPane().add(pan);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                final JButton btn = new JButton();
                this.buttons.put(btn, new Pair<Integer, Integer>(i, j));
                pan.add(btn);
                btn.addActionListener(e -> {
                    btn.setText(this.logic.hit(this.buttons.get(btn).getX(), this.buttons.get(btn).getY()) ? "*" : " ");
                    if (this.logic.isFinished()) {
                        System.exit(0);
                    }
                });
            }
        }

        this.setVisible(true);
    }

}