package a02a.e2;

import java.util.ArrayList;
import java.util.List;

public class LogicImpl implements Logic {

    private final List<Pair<Integer, Integer>> elementsHit;
    private final int size;
    private boolean finished;
    private Pair<Integer, Integer> lastElementHit;

    public LogicImpl(final int size) {
        this.elementsHit = new ArrayList<>();
        this.size = size;
        this.finished = false;
        this.lastElementHit = null;
    }

    @Override
    public boolean hit(final int x, final int y) {
        final Pair<Integer, Integer> element = new Pair<>(x, y);
        
        if (elementsHit.contains(element)) {
            elementsHit.remove(element);
            return false;
        } else {
            elementsHit.add(element);
            this.lastElementHit = element;
            return true;
        }
    }

    private void computeIfIsFinished(final Pair<Integer, Integer> element) {
        int countOnX = 0;
        int countOnY = 0;
        
        for (int i = 0; i < size; i++) {
            if (this.elementsHit.contains(new Pair<>(element.getX(), i))) {
                countOnX++;
            }

            if (this.elementsHit.contains(new Pair<>(i, element.getY()))) {
                countOnY++;
            }
        }

        if (countOnX == size || countOnY == size) {
            this.finished = true;
        }
    }

    @Override
    public boolean isFinished() {
        computeIfIsFinished(this.lastElementHit);
        return finished;
    }

}
